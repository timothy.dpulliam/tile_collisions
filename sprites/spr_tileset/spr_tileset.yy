{
    "id": "1e295120-960b-4745-b6ad-6d28f44cb4ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b13a7e3-5fac-4e29-9e01-c1f9a6119a71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e295120-960b-4745-b6ad-6d28f44cb4ae",
            "compositeImage": {
                "id": "868d9de6-830e-4ade-bf79-6854171f35b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b13a7e3-5fac-4e29-9e01-c1f9a6119a71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f4ec4bd-a3b8-4687-8955-2149a57c4862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b13a7e3-5fac-4e29-9e01-c1f9a6119a71",
                    "LayerId": "02a15fac-8af5-48b3-86ba-72310dba0451"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "02a15fac-8af5-48b3-86ba-72310dba0451",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e295120-960b-4745-b6ad-6d28f44cb4ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 226,
    "yorig": -5
}