{
    "id": "dc7f5865-df22-405c-ba46-95f466e9c0d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae4a4caf-4609-452f-91a7-a4d7055f2863",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc7f5865-df22-405c-ba46-95f466e9c0d3",
            "compositeImage": {
                "id": "b64b22d9-7269-4020-bde5-a4475131c755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae4a4caf-4609-452f-91a7-a4d7055f2863",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a81d94d-d2fb-462a-8415-62e7e1c6324d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae4a4caf-4609-452f-91a7-a4d7055f2863",
                    "LayerId": "3fec3268-2877-4373-a2de-ecfc25799d17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3fec3268-2877-4373-a2de-ecfc25799d17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc7f5865-df22-405c-ba46-95f466e9c0d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}