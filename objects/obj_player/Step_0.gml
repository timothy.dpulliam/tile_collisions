/// @desc movement logic

// get the input
var x_input = (keyboard_check(vk_right) - keyboard_check(vk_left)) * acceleration_;

// vector variables
var vector2_x = 0;
var vector2_y = 1;

// horizontal movement
velocity_[vector2_x] = clamp(velocity_[vector2_x] + x_input, -max_velocity_[vector2_x], max_velocity_[vector2_x]);


// Friction
if x_input == 0 {
	// apply 20% friction in horizontal direction. Reduce speed by 0.2 each step. 
	velocity_[vector2_x] = lerp(velocity_[vector2_x], 0, 0.2);	
}

// gravity
velocity_[vector2_y] += gravity_;

// move and contact tiles
scr_move_and_contact_tiles(collision_tile_map_id_, 64, velocity_);

// jump
var on_ground = scr_tile_collide_at_points(collision_tile_map_id_, [bbox_left, bbox_bottom], [bbox_right-1, bbox_bottom]);
if on_ground {
	// Jump
	if keyboard_check_pressed(vk_up){
		velocity_[vector2_y] = -jump_speed_;
	}
} else {
	// allow player to control jump height
	if keyboard_check_released(vk_up) and velocity_[vector2_y] <= -(jump_speed_/3){
		velocity_[vector2_y] = -(jump_speed_/3);
	}
}